<%--
  Created by IntelliJ IDEA.
  User: FPT
  Date: 7/5/2023
  Time: 1:00 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

  <title>Payment Success</title>
  <style>
    body {
      font-family: Arial, sans-serif;
      text-align: center;
      background-color: #f2f2f2;
    }

    .container {
      max-width: 500px;
      margin: 0 auto;
      padding: 20px;
      background-color: #fff;
      border-radius: 5px;
      box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
      margin-top: 100px;
    }

    h1 {
      color: #333;
    }

    p {
      color: #555;
      line-height: 1.5;
    }
  </style>
</head>
<body>
<div class="container">
  <h1>Bạn đã đặt hàng thành công!</h1>
  <p>Cảm ơn bạn đã mua hàng. Đơn hàng của bạn đã được xử lý thành công.</p>

</div>

</body>

</html>
